/*
Project G12Racer

	Team Members:
		Jaan Mohammed-Rahim
		Michael Weiner
		Mark Zhou

	Port Configurations: 
		PB4 	-> Motor
		PE3 	-> OpD
		PE4 	-> OpE
		PE5 	-> PB0
		PE6 	-> PB1
		PB0-3 	-> LCD Control
		PD0-7 	-> LCD Data
		PC0-7 	-> Keypad
		PB0-7 	-> LED
*/


/*****************
**	REGISTERS	**
*****************/
	;General use registers
		.def count 	= r16
		.def temp 	= r17
		.def temp2  = r18
	;LCD operation
		.def data 	= r19
		.def del_lo = r20
		.def del_hi = r21
	;Keypad
		.def row	= r22
		.def col	= r23
		.def mask	= r24



/*************
**	MACROS	**
*************/

;Clears a two byte word in memory, @0 is the address
.MACRO Clear
ldi r28, low(@0)
ldi r29, high(@0)
clr temp
st y+, temp
st y, temp
.ENDMACRO

;counts the number of hundreds in the first argument (a register) 
;number of tens is stored in second argument (a register)
.MACRO COUNT_THOUSANDS
	clr @1
	macro_thous_loop:
		cpi @0, 1000
		brlt end_thous_tens_loop
		inc @1
		subi @0, 1000
		rjmp macro_thous_loop
	
	end_macro_thous_loop:
.ENDMACRO


;counts the number of hundreds in the first argument (a register) 
;number of tens is stored in second argument (a register)
.MACRO COUNT_HUNDREDS
	clr @1
	macro_hundreds_loop:
		cpi @0, 100
		brlt end_macro_hundreds_loop
		inc @1
		subi @0, 100
		rjmp macro_hundreds_loop
	
	end_macro_hundreds_loop:
.ENDMACRO


;counts the number of tens in the first argument (a register) 
;number of tens is stored in second argument
.MACRO COUNT_TENS
	clr @1
	macro_tens_loop:
		cpi @0, 10
		brlt end_macro_tens_loop
		inc @1
		subi @0, 10
		rjmp macro_tens_loop
	
	end_macro_tens_loop:
.ENDMACRO


;converts register in @0 to it's single character. cannot use "temp" register as argument
.MACRO TO_CHAR
	push temp2
	ldi temp2, '0'
	add @0, temp2
	pop temp2
.ENDMACRO

;copies @1 pointed value to @0 pointed address and advance both pointers, do for @2 iterations.
.MACRO COPY_PTR_TO_PTR
	push count
	push temp
	clr count
	macro_pointer_copy_loop:
		cpi count, @2
		brge end_macro_pointer_copy_loop
		ld temp, @1+
		st @0+, temp
		inc count
		rjmp macro_pointer_copy_loop

	end_macro_pointer_copy_loop:
	pop temp
	pop count
.ENDMACRO


.include "m64def.inc"






/*****************
**	CONSTANTS	**
*****************/


;LCD protocol control bits
	.equ LCD_RS 	= 3
	.equ LCD_RW 	= 1
	.equ LCD_E 		= 2
;LCD functions
	.equ LCD_FUNC_SET 	= 0b00110000
	.equ LCD_DISP_OFF 	= 0b00001000
	.equ LCD_DISP_CLR 	= 0b00000001
	.equ LCD_DISP_ON 	= 0b00001100
	.equ LCD_ENTRY_SET 	= 0b00000100
	.equ LCD_ADDR_SET 	= 0b10000000
;LCD function bits and constants
	.equ LCD_BF 	= 7
	.equ LCD_N 		= 3
	.equ LCD_F 		= 2
	.equ LCD_ID 	= 1
	.equ LCD_S 		= 0
	.equ LCD_C 		= 1
	.equ LCD_B 		= 0
	.equ LCD_LINE1 	= 0
	.equ LCD_LINE2 	= 0x40
;LCD misc info
	.equ LCD_SCREEN_LENGTH 	= 16
	.equ RACETRACK_LENGTH 	= 8

;Keypad data	
	.equ PORTCDIR 		= 0xF0
	.equ INITCOLMASK 	= 0xEF
	.equ INITROWMASK 	= 0x01
	.equ ROWMASK 		= 0x0F

;Game state
	.equ G_WON 		= 1
	.equ G_OVER 		= 0
	.equ G_RUNNING	= 2

;Game art assets
	.equ CAR_TEXTURE		= 'C'
	.equ OBSTACLE_TEXTURE	= 'O'
	.equ POWERUP_TEXTURE	= 'P'

;Random Number Generator

	.equ RAND_A = 214013
	.equ RAND_C = 2531011

;Level time steps

	.equ lv1 = 120
	.equ lv2 = 108
	.equ lv3 = 96
	.equ lv4 = 84
	.equ lv5 = 72
	.equ lv6 = 50
	.equ lv7 = 25
	.equ lv8 = 12
	.equ lv9 = 1

/*********************
**********************
**	DATA SEGMENT	**
**********************
*********************/

.dseg

;Holds the current game screen as characters
	game_screen_line1: 	.byte 16
	game_screen_line2:	.byte 16

;Game state
	current_level: 				.byte 1
	seconds_passed_in_level: 	.byte 1

	;flags
	level_completed_flag:		.byte 1
	player_collided_flag:		.byte 1
	game_over_flag:				.byte 1		

;Player stats
	player_lives: 	.byte 1
	player_score: 	.byte 2

	;position
	player_x:		.byte 1
	player_y:		.byte 1

;Race track
	racetrack_lane1:	.byte 8
	racetrack_lane2:	.byte 8

;Keypad
	key_pressed:		.byte 1

;Timer counter
	TempCounter:		.byte 2
	step_counter:		.byte 2

;Motor wheel hole counter
	holeCounterSaved:	.byte 1

;Motor speed in rounds per second
	rpsSaved:			.byte 1

;The speed motor is set to
	setRPSSaved:		.byte 1

;Timer interval based on level (10-current_level)
	tInterval:			.byte 1

;Interval counter (counts from 0 until tInterval)
	intCounter:			.byte 1
;Random number generator

	RAND: .byte 4
;Last object placed
	lastPlaced: .byte 1

;Motorflag
	motorFlag: .byte 1

;Debug flag
	debug:    	.byte 1

/*********************
**********************
**	CODE SEGMENT	**
**********************
*********************/

.cseg
.org 0x0000
jmp RESET
jmp EXT_INT0 ; IRQ0 Handler
jmp Default ; IRQ1 Handler
jmp EXT_INT2 ; IRQ2 Handler
jmp Default ; IRQ3 Handler
jmp Default ; IRQ4 Handler
jmp Default ; IRQ5 Handler
jmp Default ; IRQ6 Handler
jmp Default ; IRQ7 Handler
jmp Default ; Timer2 Compare Handler
jmp Timer2 ; Timer2 Overflow Handler
jmp Default ; Timer1 Capture Handler
jmp Default ; Timer1 CompareA Handler
jmp Default ; Timer1 CompareB Handler
jmp Default ; Timer1 Overflow Handler
jmp Default ; Timer0 Compare Handler
jmp Default ; Timer0 Overflow Handler

Default: reti

/*****************************************
**	DEDICATED PROGRAM MEMORY DIRECTIVES	**
*****************************************/

;Default game screen
	default_game_top_row: 	.db "L:  C: |        "
	default_game_bot_row: 	.db "S:     |        "   

;Game icons
	player_icon: 	.db "C "
	obstacle_icon:	.db "O "
	powerup_icon:	.db "P "


RESET: 
	; Initialize stack pointer
	ldi temp, high(RAMEND) 	
	out SPH, temp
	ldi temp, low(RAMEND)
	out SPL, temp

	;set up LCD data port, output
	ser temp
	out DDRA, temp

	;set up keypad port, columns are outputs, rows are inputs
	ldi temp, PORTCDIR 
	out DDRC, temp
	;clear key_pressed, used for controlling input from keypad
	clr temp
	sts key_pressed, temp

	out DDRB, temp ;Setting up motor PWM pin for output

	ldi temp, (1 << PD1)
	out DDRD, temp	;Setting up motor emitter pin for output
	out PORTD, temp

	;setting the interrupt for falling edge
	ldi temp, (2 << ISC00) 
	sts EICRA, temp
	in temp, EIMSK
	ori temp, (1<<INT0)
	out EIMSK, temp
	
	;setting the interrupt for falling edge
	ldi temp, (2 << ISC20)
	sts EICRA, temp
	in temp, EIMSK
	ori temp, (1<<INT2)
	out  EIMSK, temp

	;set up PWM for compare match
	ldi temp, 0b01101001 
	out TCCR0, temp
	ldi temp, 0x00
	out OCR0, temp
	
	;set up timer1
	
	ldi r17, 1 << CS20 
	out TCCR1B, r17

	;set up timer2
	Clear TempCounter 
	ldi temp, 0b00000010
	out TCCR2, temp
	ldi temp, 1<<TOIE2
	out TIMSK, temp
	ldi temp, 200 ;sets the default rps as 70rps
	sts setRPSSaved, temp
	
	ldi temp, 0
	sts motorFlag, temp
		
	; enable interrupts
	sei

	rjmp main


/*****************
**	FUNCTIONS	**
*****************/

/*------------
--	Timer 2	--
------------*/


Timer2:
	push temp
	in temp, SREG ;Prologue
	push temp
	push temp2
	push r29
	push r28
	push r27
	push r26
	push r31
	push r30
	push r25
	push r24 ;End Prologue
	
	;DO NOTHING IF IN GAME OVER
	/*
	ldi ZL, low(game_over_flag)
	ldi ZH, high(game_over_flag)
	ld temp, Z
	cpi temp, 0
	brne end_timer2*/
		ldi temp,1
		sts debug, temp
		ldi r28, low(TempCounter) ;load address of counter
		ldi r29, high(TempCounter)
		ld r24, y+  ;load the value of the tempCounter
		ld r25, y
		adiw r25:r24, 1 ;increment tempCounter
		
		cpi r24, low(120) ;checks if timer is equal to a second
		cpc r25, temp
		brne normal_timestep
		//always fails this test
			//rcall tasks_every_second
				;motor spinna
			lds temp, motorFlag
			cpi temp, 1
			brsh skip_motor

				ldi temp, 0x00
				out OCR0, temp
				rjmp normal_timestep

			skip_motor:
			ldi temp, 0xBB
			out OCR0, temp
			lds temp, motorFlag
			dec temp
			sts motorFlag, temp

		normal_timestep:		
		lds temp2, current_level

		cpi temp2, 1
		brne try2
		cpi r24, low(lv1) ;checks if timer is equal to a second
		ldi temp, high(lv1)
		rjmp end_increment

		try2:
		cpi temp2, 2
		brne try3
		cpi r24, low(lv2) ;checks if timer is equal to 0.9
		ldi temp, high(lv2)
		rjmp end_increment

		try3:
		cpi temp2, 3
		brne try4
		cpi r24, low(lv3) ;checks if timer is equal to 0.8
		ldi temp, high(lv3)
		rjmp end_increment

		try4:
		cpi temp2, 4
		brne try5
		cpi r24, low(lv4) ;checks if timer is equal to a 0.7
		ldi temp, high(lv4)
		rjmp end_increment
	
		try5:
		cpi temp2, 5
		brne try6
		cpi r24, low(lv5) ;checks if timer is equal to a 0.6
		ldi temp, high(lv5)
		rjmp end_increment
	
		try6:
		cpi temp2, 6
		brne try7
		cpi r24, low(lv6) ;checks if timer is equal to 0.5
		ldi temp, high(lv6)
		rjmp end_increment
	
		try7:
		cpi temp2, 7
		brne try8
		cpi r24, low(lv7) ;checks if timer is equal to 0.4
		ldi temp, high(lv7)
		rjmp end_increment

		try8:
		cpi temp2, 8
		brne try9
		cpi r24, low(lv8) ;checks if timer is equal to 0.3
		ldi temp, high(lv8)
		rjmp end_increment

		try9:
		cpi temp2, 9
		brne try0
		cpi r24, low(lv9) ;checks if timer is equal to 0.2
		ldi temp, high(lv9)
		rjmp end_increment
	
		try0:
		cpi r24, low(lv1) ;resets timestep
		ldi temp, high(lv1)
		rjmp end_increment

		end_increment:
		cpc r25, temp
		brne not_second
			
			is_timestep:
			//debug
			rcall tasks_every_second

				rcall timestep_tasks 
	
			Clear TempCounter ;reset tempCounter
			rjmp end_timer2
	
		not_second:
		st y, r25
		st -y, r24

	end_timer2:
	ldi temp, 0
			sts debug, temp
	pop r24
	pop r25
	pop r30
	pop r31
	pop r26
	pop r27
	pop r28
	pop r29
	pop temp2
	pop temp
	out SREG, temp
	pop temp

	reti

		timestep_tasks:
			push temp
			push count
			push temp2
			
			rcall racetrack_erase_player
			rcall struct_advance_racetrack
			;check for collision after shift
			rcall check_post_advance_collision
			rcall place_object
			pop temp2
			pop count
			pop temp
			ret
		
			check_post_advance_collision:
			
				;check lane of player
				ldi XL, low(player_y)
				ldi XH, high(player_y)
				ld temp, X
				cpi temp, 0
				brne check_collision_ln2
	
				check_collision_ln1:
					ldi XL, low(racetrack_lane1)
					ldi XH, high(racetrack_lane1)
					rjmp in_correct_chk_lane

				check_collision_ln2:
					ldi XL, low(racetrack_lane2)
					ldi XH, high(racetrack_lane2)
					rjmp in_correct_chk_lane

				in_correct_chk_lane:
				ldi ZL, low(player_x)
				ldi ZH, high(player_x)
				ld temp, Z
				add XL, temp

				;now in exact position of player before draw, check for collidable
				ld temp, X
				rcall collectible_check
	
				
				ret

		;function to place an obstacle or a powerup at the end of the racetrack
		place_object:
			;place a space at the end of the racetrack
			push r17
			push temp2
			ldi temp2, ' '
			ldi ZL, low( racetrack_lane1+7)	
			ldi	ZH, high( racetrack_lane1+7)
			st Z, temp2
			ldi ZL, low( racetrack_lane2+7)	
			ldi	ZH, high( racetrack_lane2+7)
			st Z, temp2
			
			;check if an obstacle was last placed
			lds r17, lastPlaced
			cpi r17, 1
			breq turnOff
			clr r17
				rcall GetRandom

			modulo:
				cpi r17, 20
				brlo topPowerup
				subi r17, 20
				rjmp modulo

			topPowerup:

				ldi temp2, POWERUP_TEXTURE
				cpi r17, 0
				brne bottomPowerup
				rjmp laneOne

			bottomPowerup:	
				cpi r17, 1
				brne topObstacle
				rjmp laneTwo

			topObstacle:
				ldi temp2,OBSTACLE_TEXTURE
				cpi r17, 7
				brge bottomObstacle
				ldi r17, 1
				sts lastPlaced, r17
				rjmp laneOne

			bottomObstacle:
				cpi r17, 12
				brge noPlacement
				ldi r17, 1
				sts lastPlaced, r17
				rjmp laneTwo

			laneOne:
				ldi ZL, low( racetrack_lane1+7)	
				ldi	ZH, high( racetrack_lane1+7)
				st Z, temp2
				rjmp noPlacement

			laneTwo:
				ldi ZL, low( racetrack_lane2+7)	
				ldi	ZH, high( racetrack_lane2+7)
				st Z, temp2
				rjmp noPlacement

			turnOff:
				ldi r17, 0
				sts lastPlaced, r17

			noPlacement: 
				pop temp2
				pop r17
				ret
			
	

		tasks_every_second:

			;increment seconds passed in level
			lds temp, seconds_passed_in_level			
			inc temp
			sts seconds_passed_in_level, temp			
			
			;check if thirty seconds have passed
			cpi temp, 30
			brlt not_thirty_seconds
				;set up level complete flag
				ldi temp, 1
				sts level_completed_flag, temp
				clr temp
				sts seconds_passed_in_level, temp
			not_thirty_seconds:
			
	

			ret
		
EXT_INT0:
	push temp
	in temp, SREG
	push temp

	;after a hole in the wheel is registered, increment the 
	;hole counter and save to data memory.
	lds temp, holeCounterSaved
	inc temp
	sts holeCounterSaved, temp

	pop temp
	out SREG, temp
	pop temp
	reti

/*----------------------------
--	RANDOM NUMBER GENERATOR	--
----------------------------*/

InitRandom:
	push r17 ; save conflict register

	in r17, TCNT1L ; Create random seed from time of timer 1
	sts RAND,r17
	sts RAND+2,r17
	in r17,TCNT1H
	sts RAND+1, r17
	sts RAND+3, r17

	pop r17 ; restore conflict register
	ret


GetRandom:
	push r0 ; save conflict registers
	push r1
	push r23
	push r18
	push r19
	push r20
	push r21
	push r22

	clr r22 ; remains zero throughout

	ldi r17, low(RAND_C) ; set original value to be equal to C
	ldi r23, BYTE2(RAND_C)
	ldi r18, BYTE3(RAND_C)
	ldi r19, BYTE4(RAND_C)

	; calculate A*X + C where X is previous random number.  A is 3 bytes.
	lds r20, RAND
	ldi r21, low(RAND_A)
	mul r20, r21 ; low byte of X * low byte of A
	add r17, r0
	adc r23, r1
	adc r18, r22

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; low byte of X * middle byte of A
	add r23, r0
	adc r18, r1
	adc r19, r22

	ldi r21, byte3(RAND_A)
	mul r20, r21  ; low byte of X * high byte of A
	add r18, r0
	adc r19, r1

	lds r20, RAND+1
	ldi r21, low(RAND_A)
	mul r20, r21  ; byte 2 of X * low byte of A
	add r23, r0
	adc r18, r1
	adc r19, r22

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; byte 2 of X * middle byte of A
	add r18, r0
	adc r19, r1

	ldi r21, byte3(RAND_A)
	mul r20, r21  ; byte 2 of X * high byte of A
	add r19, r0

	lds r20, RAND+2
	ldi r21, low(RAND_A)
	mul r20, r21  ; byte 3 of X * low byte of A
	add r18, r0
	adc r19, r1

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; byte 2 of X * middle byte of A
	add r19, r0

	lds r20, RAND+3
	ldi r21, low(RAND_A)	
	mul r20, r21  ; byte 3 of X * low byte of A
	add r19, r0

	sts RAND, r17 ; store random number
	sts RAND+1, r23
	sts RAND+2, r18
	sts RAND+3, r19

	mov r17, r19  ; prepare result (bits 30-23 of random number X)
	lsl r18
	rol r17

	pop r22 ; restore conflict registers
	pop r21 
	pop r20
	pop r19
	pop r18
	pop r23
	pop r1
	pop r0
	ret



/*--------
--	LCD	--
--------*/

; The del_hi:del_lo register pair store the loop counts
; each loop generates about 1 us delay
lcd_delay:
	delay_loop:
		subi del_lo, 1
		sbci del_hi, 0
		nop
		nop
		nop
		nop
		brne delay_loop
	; taken branch takes two cycles.
	; one loop time is 8 cycles = ~1.08us
	ret

;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
	out porta, data ; set the data port's value up
	clr temp
	out portb, temp ; RS = 0, RW = 0 for a command write
	nop ; delay to meet timing (Set up time)
	sbi portb, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi portb, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
lcd_write_data:
	out porta, data ; set the data port's value up
	ldi temp, 1 << LCD_RS
	out portb, temp ; RS = 1, RW = 0 for a data write
	nop ; delay to meet timing (Set up time)
	sbi portb, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi portb, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
	clr temp
	out ddra, temp ; Make porta be an input port for now
	out porta, temp
	ldi temp, 1 << LCD_RW
	out portb, temp ; RS = 0, RW = 1 for a command port read
	busy_loop:
		nop ; delay to meet timing (Set up time / Enable cycle time)
		sbi portb, LCD_E ; turn on the enable pin
		nop ; delay to meet timing (Data delay time)
		nop
		nop
		in temp, pina ; read value from LCD
		cbi portb, LCD_E ; turn off the enable pin
		sbrc temp, LCD_BF ; if the busy flag is set
		rjmp busy_loop ; repeat command read
		clr temp ; else
		out portb, temp ; turn off read mode,
		ser temp
		out ddra, temp ; make portd an output port again
	ret ; and return
; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
; must be delayed. Actual delay will be slightly greater (~1.08us*r18:r19).
; r18:r19 are altered in this function.
; Code is omitted

;Function lcd_init Initialisation function for LCD.
lcd_init:
	ser temp
	out ddra, temp ; portd, the data port is usually all otuputs
	out ddrb, temp ; portb, the control port is always all outputs
	ldi del_lo, low(15000)
	ldi del_hi, high(15000)
	rcall lcd_delay ; delay for > 15ms
	; Function set command with N = 1 and F = 0
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font
	ldi del_lo, low(4100)
	ldi del_hi, high(4100)
	rcall lcd_delay ; delay for > 4.1ms
	rcall lcd_write_com ; 2nd Function set command with 2 lines and 5*7 font
	ldi del_lo, low(100)
	ldi del_hi, high(100)
	rcall lcd_delay ; delay for > 100us
	rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
	rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_OFF
	rcall lcd_write_com ; Turn Display off
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_CLR
	rcall lcd_write_com ; Clear Display
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Entry set command with I/D = 1 and S = 0
	ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
	rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Display on command with C = 0 and B = 1
	ldi data, LCD_DISP_ON | (1 << LCD_C)
	rcall lcd_write_com ; Turn Display on with a cursor that doesn't blink
	ret

;Function to write from 'data' register to next space in LCD
lcd_write_next:
	rcall lcd_wait_busy
    rcall lcd_write_data            ; write the character to the screen
	ret
     
;Function to set writer to Line2 in LCD
lcd_go_line2:
	rcall lcd_wait_busy
    ldi data, LCD_ADDR_SET | LCD_LINE2
    rcall lcd_write_com                     ; move the insertion point to start of line 2
	ret


/*----------------
--	Draw game	--
----------------*/

;Function to draw the entire game screen to LCD
draw_game:
	;clear screen
	rcall lcd_init
	
	;draw line 1
	ldi ZL, low( game_screen_line1 )	
	ldi	ZH, high( game_screen_line1 )
	rcall lcd_draw_line
	
	;draw line 2
	rcall lcd_go_line2
	ldi ZL, low( game_screen_line2 )	
	ldi	ZH, high( game_screen_line2 )
	rcall lcd_draw_line

	;put score in LEDs
	rcall score_to_led

	ret
	
	;Function used by draw_game to draw entire LCD line
	; assumes Z is pointing to a game_screen_lineX
	lcd_draw_line:
		push count

		clr count
		draw_loop_again:
			;write a character, point to next to be written
			ld data, Z+
			rcall lcd_write_next
			
			;check if 16 have been written
			inc count
			cpi count, LCD_SCREEN_LENGTH
			brge end_draw_loop

			rjmp draw_loop_again

		end_draw_loop:
		pop count
		ret
	
	;puts the current player score into the LEDs
	score_to_led:
		in temp, ddrb
		push temp
		ser temp
		out ddrb, temp
		ldi XL, low(player_score)
		ldi XH, high(player_score)
		ld temp, X
		out portb, temp
		pop temp
		out ddrb, temp				

		ret


/*--------------------
--	Game structure	--
--------------------*/

;function to initialise game structure and values
game_structure_init:
	push temp
	push temp2

	;set the default screen setup
	rcall game_screen_init
	
	;seeds random timer
	rcall InitRandom
	
	;turn on timer
	ldi temp, 1<<TOIE2
	out TIMSK, temp

	;set the default player level, lives, score
	ldi temp, 1
	sts current_level, temp
	ldi temp, 3
	sts player_lives, temp
	clr temp
	ldi temp, 0
	sts player_score, temp

	rcall init_level
	
	pop temp2
	pop temp
	ret
	
	;function to initialise game screen
	game_screen_init:
		
		;initialise the screen
		rcall lcd_init	
	
		ldi XL, low( game_screen_line1 )	
		ldi	XH, high( game_screen_line1 )
	
		ldi ZL, low( default_game_top_row << 1 )
		ldi	ZH, high( default_game_top_row << 1 )
	
		rcall game_screen_init_loop

		ldi XL, low( game_screen_line2 )	
		ldi	XH, high( game_screen_line2 )
	
		ldi ZL, low( default_game_bot_row << 1 )
		ldi	ZH, high( default_game_bot_row << 1 )
	
		rcall game_screen_init_loop

		ret
	
		;function used in screen initialisation to copy a line of the default screen to the structure
		;assumes Z at default game and X at game screen line
		game_screen_init_loop:
			push count

			clr count
			game_screen_init_loop_again:
				lpm data, Z+
				st X+, data

				rcall lcd_write_next

				inc count
				cpi count, LCD_SCREEN_LENGTH
				brge end_game_screen_init_loop

				rjmp game_screen_init_loop_again

			end_game_screen_init_loop:
			pop count

			ret
	
		;function to clear the racetrack 
		game_racetrack_init:
			;writing spaces to the racetrack
			ldi temp, ' '

			ldi XL, low( racetrack_lane1 )	
			ldi XH, high( racetrack_lane1 )
			
			clr count
			erasing_racetrack_line1:
				st X+, temp
				inc count
				cpi count, 9
				brge begin_erasing_racetrack_line2
				rjmp erasing_racetrack_line1

						
			begin_erasing_racetrack_line2:

			ldi XL, low(racetrack_lane2)
			ldi XH, high(racetrack_lane2)
			
			clr count
			erasing_racetrack_line2:
				st X+, temp
				inc count
				cpi count, 9
				brge end_racetrack_init
				rjmp erasing_racetrack_line2
			
			
			end_racetrack_init:

			ret


		init_level:
	
			;set default player position
			clr temp
			sts player_x, temp
			sts player_y, temp

			;reset second passed in this level to zero
			clr temp
			sts seconds_passed_in_level, temp

			;clear racetrack
			rcall game_racetrack_init
			
			;clear all flags
			clr temp
			sts level_completed_flag, temp
			sts player_collided_flag, temp
			sts game_over_flag, temp		
			
			;set default game speed based on level
			lds temp, current_level
			subi temp, 1
			ldi temp2, 10
			sub temp2, temp
			sts tInterval, temp2
			mov temp, temp2
			add temp2, temp
			clr temp
			sts intCounter, temp
			
			ret


struct_update:
	push temp
	push count
	push temp2

	rcall struct_update_level_screen
	rcall struct_update_lives_screen
	rcall struct_update_score_screen
	rcall struct_update_racetrack
	
	rcall check_game_state

	pop temp2
	pop count
	pop temp
	ret

;check the game state, update as necessary
check_game_state:
	
	;check for collision occurence
	lds temp, player_collided_flag
	cpi temp, 0
	breq player_not_collided
		rcall player_collision_tasks

	player_not_collided:

	;check if level is complete occurence
	lds temp, level_completed_flag
	cpi temp, 0
	breq level_not_complete
		rcall level_completed_flag_tasks	

	level_not_complete:
	
	;check for game over
	lds temp, game_over_flag
	cpi temp, 0
	breq game_not_over
		rcall game_over_tasks
		
	game_not_over:

	ret
	
	level_completed_flag_tasks:
		;increase the level
		lds temp, current_level
		inc temp
		sts current_level, temp

		;change timestep
		//TODO
		
		;clear the flag
		clr temp
		sts level_completed_flag, temp

		rcall init_level
		ret

	player_collision_tasks:

		ldi temp, 2
		sts motorFlag, temp
		lds temp, player_lives
		dec temp
		
		;check if player still has lives left
		cpi temp, 0
		brne still_alive
			ldi temp, 1
			sts game_over_flag, temp
			rjmp 	end_player_collision_tasks
		
		still_alive:
		sts player_lives, temp

		;reset the level
		rcall init_level
	
		end_player_collision_tasks:
		;clear the flag
		clr temp
		sts player_collided_flag, temp

		ret


struct_update_level_screen:
	push temp
	push count	
	
	lds count, current_level
	
	COUNT_TENS count, temp
	TO_CHAR temp
	sts game_screen_line1+2, temp	;write the tens of the current level to 2 bytes ahead of game_screen_line1

	TO_CHAR count
	sts game_screen_line1+3, count ;write the current level to 3 bytes ahead of game_screen_line1
	
	pop count
	pop temp
	ret

struct_update_lives_screen:
	push count

	lds count, player_lives
	TO_CHAR count
	sts game_screen_line1+6, count
	
	pop count
	ret

struct_update_score_screen:
	push temp
	push count
	
	lds count, player_score
	
	COUNT_HUNDREDS count, temp
	TO_CHAR temp
	sts game_screen_line2+4, temp	;write the hundreds of the current level to 2 bytes ahead of game_screen_line1

	COUNT_TENS count, temp
	TO_CHAR temp
	sts game_screen_line2+5, temp	;write the tens of the current level to 2 bytes ahead of game_screen_line1

	TO_CHAR count
	sts game_screen_line2+6, count ;write the current level to 3 bytes ahead of game_screen_line1
	
	pop count
	pop temp
	ret

struct_update_racetrack:
	
	rcall struct_update_player

	;copy the current state of the racetrack to the racetrack part of the game screen
	rcall struct_racetrack_to_screen
	
	ret

struct_update_player: 
	push temp

	rcall racetrack_erase_player
	
	;check which lane player is in, will erase from there

	rcall struct_X_to_player_racetrack
	ldi temp, CAR_TEXTURE
	st X, temp 

	end_update_player:
	
	pop temp
	ret

;moves the X pointer to the exact position of the player on the racetrack
;needs temp, X and Z
struct_X_to_player_racetrack:
	push temp
	
	ldi ZL, low(player_y)
	ldi ZH, high(player_y)
	ld temp, Z
	cpi temp, 1
	breq struct_X_to_player_racetrack_player_in_line2

		struct_X_to_player_racetrack_player_in_line1:
		ldi XL, low(racetrack_lane1)
		ldi XH, high(racetrack_lane1)
		rjmp struct_X_to_player_racetrack_in_correct_lane	

		struct_X_to_player_racetrack_player_in_line2:
		ldi XL, low(racetrack_lane2)
		ldi XH, high(racetrack_lane2)
		rjmp struct_X_to_player_racetrack_in_correct_lane
	
	struct_X_to_player_racetrack_in_correct_lane:		
	;put player_x position in temp, increment X pointer by this amount
	ldi ZL, low(player_x)
	ldi ZH, high(player_x)
	ld temp, Z
	add XL, temp		
	
	struct_X_to_player_racetrack_end_update_player:
	
	pop temp
	ret

	

struct_racetrack_to_screen:
	
	;put lane 1 of racetrack in game screen
	ldi ZL, low( game_screen_line1 + 8 )	
	ldi	ZH, high( game_screen_line1  + 8)
	ldi XL, low(racetrack_lane1)
	ldi XH, high(racetrack_lane1)
	COPY_PTR_TO_PTR Z, X, 8
	
	;put lane 2 of racetrack in game screen
	ldi ZL, low( game_screen_line2 + 8)	
	ldi	ZH, high( game_screen_line2 + 8)
	ldi XL, low(racetrack_lane2)
	ldi XH, high(racetrack_lane2)
	COPY_PTR_TO_PTR Z, X, 8

	;clear old position of player from racetrack
	rcall racetrack_erase_player
	
	ret

;moves the entire racetrack forward once
struct_advance_racetrack:
	push temp
	push temp2
	
	;remove any powerups from halfway
	ldi XL, low(racetrack_lane1 + 4)
	ldi XH, high(racetrack_lane1 + 4)
	ld temp, X
	cpi temp, POWERUP_TEXTURE
	brne check_lane2_powerup
		ldi temp, ' '
		st X, temp
		rjmp start_racetrack_advance

	check_lane2_powerup:
	ldi XL, low(racetrack_lane2 + 4)
	ldi XH, high(racetrack_lane2 + 4)
	ld temp, X
	cpi temp, POWERUP_TEXTURE
	brne start_racetrack_advance
		ldi temp, ' '
		st X, temp
	
	start_racetrack_advance:
	;point to beginning of lane 1 and byte after that for Z and X respectively
	ldi ZL, low(racetrack_lane1)	
	ldi	ZH, high(racetrack_lane1)
	ldi XL, low(racetrack_lane1 + 1)
	ldi XH, high(racetrack_lane1 + 1)
		
	;check if an obstacle is about to leave the track on lane 1
	ld temp, Z
	cpi temp, OBSTACLE_TEXTURE
	brne no_obstacle_in_lane1
			lds temp2, player_score
			lds temp, current_level
			add temp, temp2
			sts player_score, temp

	no_obstacle_in_lane1:
	
	;copy X into Z 7 times
	rcall advance_pointer_copy_pointer
	
	;clear last space
	
	ldi XL, low(racetrack_lane1 + 7)
	ldi XH, high(racetrack_lane1 + 7)
	ldi temp, ' '
	st X, temp

	;point to beginning of lane 2 and byte after that for Z and X respectively
	ldi ZL, low(racetrack_lane2)	
	ldi	ZH, high(racetrack_lane2)
	ldi XL, low(racetrack_lane2 + 1)
	ldi XH, high(racetrack_lane2 + 1)

	;check if an obstacle is about to leave the track on lane 2
	ld temp, Z
	cpi temp, OBSTACLE_TEXTURE
	brne no_obstacle_in_lane2
		lds temp2, player_score
		lds temp, current_level
		add temp, temp2
		sts player_score, temp
	
	no_obstacle_in_lane2:

	;copy X into Z 7 times
	rcall advance_pointer_copy_pointer
	;clear last space
	ldi XL, low(racetrack_lane2 + 7)
	ldi XH, high(racetrack_lane2 + 7)
	ldi temp, ' '
	st X, temp
	
	pop temp2
	pop temp
	
	ret
	
	;assumes Z and X are at the 0 and 1 columns of a racetrack lane
	advance_pointer_copy_pointer:	
		push count
		push temp

		clr count
		advance_pointer_copy_loop:
			cpi count, 7
			brge end_advance_pointer_copy_loop
			ld temp, X+
			st Z+, temp
			inc count
			rjmp advance_pointer_copy_loop

		end_advance_pointer_copy_loop:
		pop temp
		pop count

		ret
/*--------------------
--	Player movement	--
--------------------*/

collect_powerup_tasks:
	ldi XL, low(current_level)
	ldi XH, high(current_level)
	ld temp, X
	ldi count, 10
	mul temp, count
	mov temp, r0
	ldi ZL, low(player_score)
	ldi ZH, high(player_score)
	ld count, Z

	add count, temp
	st Z, count
	ret

;the target tile needs to be in 'temp'
collectible_check:
	
	cpi temp, OBSTACLE_TEXTURE
	brne not_obs_collectible
		ldi temp, 1
		sts player_collided_flag, temp
		rjmp end_collectible_check

	not_obs_collectible:
	cpi temp, POWERUP_TEXTURE
	brne not_powerup_clctble
		rcall collect_powerup_tasks
		rjmp end_collectible_check

	not_powerup_clctble:


	end_collectible_check:

	ret

player_move_north:
	push temp
	
	;check if player is already in top lane
	lds temp, player_y
	cpi temp, 1
	;do nothing if true
	brlt end_north
		ldi ZL, low(racetrack_lane1)	
		ldi	ZH, high(racetrack_lane1)
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ld temp, X
		add ZL, temp
		ld temp, Z

		rcall collectible_check

		end_north:

		ldi temp, 0
		sts player_y, temp
	
		rjmp end_player_move
	
player_move_south:
	push temp

	;check if player is already in bottom lane
	lds temp, player_y
	cpi temp, 1
	;do nothing if true
	brge end_south
	
		ldi ZL, low(racetrack_lane2)	
		ldi	ZH, high(racetrack_lane2)
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ld temp, X
		add ZL, temp
		ld temp, Z
		cpi temp, OBSTACLE_TEXTURE

		rcall collectible_check
		
		end_south:

		ldi temp, 1
		sts player_y, temp
	
		rjmp end_player_move
	
player_move_east:
	push temp

	;check if player is already at far east
	lds temp, player_x
	cpi temp, 7
	;do nothing if true
	brge end_player_move
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ldi YL, low(player_y)
		ldi YH, high(player_y)
		ld temp, Y
		cpi temp, 0
		brne southEast
			ldi ZL, low(racetrack_lane1)	
			ldi	ZH, high(racetrack_lane1)
			rjmp eastCheck
		southEast:

			ldi ZL, low(racetrack_lane2)	
			ldi	ZH, high(racetrack_lane2)

		eastCheck:
		ld temp, X
		ldi temp2, 1
		add temp, temp2
		add ZL, temp
		ld temp, Z
		
		rcall collectible_check

	
		;move player east once otherwise

		notEast:
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ld temp, X
		inc temp
		sts player_x, temp
	
		rjmp end_player_move
	
player_move_west:
	push temp

	;check if player is already at far west
	lds temp, player_x
	cpi temp, 0
	;do nothing if true
	breq end_player_move
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ldi YL, low(player_y)
		ldi YH, high(player_y)
		ld temp, Y
		cpi temp, 0
		brne southWest
		ldi ZL, low(racetrack_lane1)	
		ldi	ZH, high(racetrack_lane1)
		rjmp westCheck
		southWest:

		ldi ZL, low(racetrack_lane2)	
		ldi	ZH, high(racetrack_lane2)

		westCheck:
		ld temp, X
		subi temp, 1
		add ZL, temp
		ld temp, Z

		rcall collectible_check

		;move player east once otherwise

		notWest:
		ldi XL, low(player_x)
		ldi XH, high(player_x)
		ld temp, X
		dec temp
		sts player_x, temp
	
		rjmp end_player_move
	
	;used for all player move commands, assumes temp was last pushed
	end_player_move:
	
	pop temp
	ret
	
;function to erase player from racetrack
racetrack_erase_player:
	push temp
	push temp2
	push count
	
	ldi temp2, ' '
	rcall struct_X_to_player_racetrack
	st X, temp2
	
	erase_player_end:

	pop count
	pop temp2
	pop temp
	ret


/*------------
--	Keypad	--
------------*/

;function to scan from the keypad
scan_keypad:
	push temp
	push temp2

	ldi mask, INITCOLMASK ; initial column mask
	clr col ; initial column

	colloop:
		out PORTC, mask ; set column to mask value
		; (sets column 0 off)
		ldi temp, 0xFF ; implement a keypad_delay so the

	; hardware can stabilize
	keypad_delay:
		dec temp
		brne keypad_delay
		in temp, PINC ; read PORTC
		andi temp, ROWMASK ; read only the row bits
		cpi temp, 0xF ; check if any rows are grounded
		breq nextcol ; if not go to the next column
		ldi mask, INITROWMASK ; initialise row check
		clr row ; initial row

	rowloop:
		mov temp2, temp
		and temp2, mask ; check masked bit
		brne skipconv ; if the result is non-zero,
		; we need to look again
		rcall convert_to_binary ; if bit is clear, convert the bitcode
		
	end_scan_keypad:
		pop temp2
		pop temp
		ret ;return from scan_keypad

		skipconv:
			inc row ; else move to the next row
			lsl mask ; shift the mask to the next bit
			jmp rowloop

	nextcol:

		cpi col, 3 ; check if we�re on the last column
		breq key_null_pressed;if not, no buttons pushed, so return from keypad scanning

		sec ; else shift the column mask:
		; We must set the carry bit
		rol mask ; and then rotate left by a bit,
		; shifting the carry into
		; bit keypad_zero. We need this to make
		; sure all the rows have
		; pull-up resistors
		inc col ; increment column value
		jmp colloop ; and check the next column

	key_null_pressed:
		;since we are sure no button has been pressed, we clear key_pressed
		clr temp
		sts key_pressed, temp
		rjmp end_scan_keypad

	; convert_to_binary function converts the row and column given to a
	; binary number and also outputs the value to PORTA.
	; Inputs come from registers row and col and output is in
	; temp.
	convert_to_binary:
		push temp

		cpi col, 3 ; if column is 3 we have a letter
		breq keypad_letter
		cpi row, 3 ; if row is 3 we have a symbol or 0
		breq keypad_symbol
		mov temp, row ; otherwise we have a number (1-9)
		lsl temp ; temp = row * 2
		add temp, row ; temp = row * 3
		add temp, col ; add the column address
		; to get the offset from 1
		inc temp ; add 1. Value of switch is
		; row*3 + col + 1.
		jmp convert_to_binary_end

	keypad_letter:
		ldi temp, 0xA
		add temp, row ; increment from 0xA by the row value
		jmp convert_to_binary_end

	keypad_symbol:
		cpi col, 0 ; check if we have a keypad_star
		breq keypad_star
		cpi col, 1 ; or if we have keypad_zero
		breq keypad_zero
		ldi temp, 0xF ; we'll output 0xF for hash
		jmp convert_to_binary_end

	keypad_star:
		ldi temp, 0xE ; we'll output 0xE for keypad_star
		jmp convert_to_binary_end
	keypad_zero:
		clr temp ; set to keypad_zero
		jmp convert_to_binary_end


	
	;by this point, in "temp" register is value of button pressed
	convert_to_binary_end:
		push temp2

		;check if a key is still held down, and ignore this press if it is.
		lds temp2, key_pressed
		cpi temp2, 0
		brne post_key_command ;ignore if still pressed		

		;now we act on button pressed
		rcall do_keypad_command	
			
		;now we are sure a button has been pressed, so set key_pressed to non-zero
		ser temp
		sts key_pressed, temp

		post_key_command:
		pop temp2
		pop temp
		ret ; return from convert_to_binary



;function to act based on command from keypad, button press value should be in "temp"
do_keypad_command:
	;key 2: move north
	cpi temp, 2
	brne not_2
		rcall player_move_north	
		rjmp end_do_keypad_command

	not_2:
	;key 4: move west
	cpi temp, 4
	brne not_4
		rcall player_move_west
		rjmp end_do_keypad_command
	
	not_4:
	;key 6: move east
	cpi temp, 6
	brne not_6
		rcall player_move_east
		rjmp end_do_keypad_command
	
	not_6:
	;key 8: move south
	cpi temp, 8
	brne not_8
		rcall player_move_south
		rjmp end_do_keypad_command

	not_8:

	not_key:


	end_do_keypad_command:
	
	ret

/*----------------
--	Motor Run	--
----------------*/


/*************
**************
**	MAIN 	**
**************
*************/


main: 
	EXT_INT2:
	rcall game_structure_init
	
	main_loop:
		
		rcall scan_keypad
		rcall struct_update
		cli
		rcall draw_game
		sei
		
		rjmp main_loop              ; infinite loop


game_over_tasks:
		; end game, draw X to player, infinite loop, pause timer in loop, wait for reset
		;rcall struct_X_to_player_racetrack
		;ldi temp, 'X'
		;st X, temp
		ldi temp, 0<<TOIE2
		out TIMSK, temp
		;rcall struct_update
		;rcall draw_game

		game_over_loop:
			

			rjmp game_over_loop
		
		ret

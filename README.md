RacerGame

	Coded by:
		Jaan Mohammed-Rahim
		Michael Weiner
		Mark Zhou

	Port Configurations for ATmega2560 Microcontroller: 
		PB4 	-> Motor
		PE3 	-> OpD
		PE4 	-> OpE
		PE5 	-> PB0
		PE6 	-> PB1
		PB0-3 	-> LCD Control
		PD0-7 	-> LCD Data
		PC0-7 	-> Keypad
		PB0-7 	-> LED